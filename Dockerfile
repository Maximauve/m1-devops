FROM php:8.1.20-apache
WORKDIR /var/www/html/
COPY src/ /var/www/html/
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]